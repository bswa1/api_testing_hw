import { ApiRequest } from "../request";

export class RegisterController {
    async register(id: number, avatar: string, email: string, password: string, userName: string) {
        const response = await new ApiRequest()
        .prefixUrl("http://tasque.lol/")
        .method("POST")
        .url('api/Register')
        .body({
            "id": id,
            "avatar": avatar,
            "email": email,
            "password": password,
            "username": userName
        })
        .send();
        return response;
    }
}
