import { ApiRequest } from "../request";

export class AuthController {
    async login(email: string, password: string) {
        const response = await new ApiRequest()
            .prefixUrl("http://tasque.lol/")
            .method("POST")
            .url(`api/Auth/login`)
            .body({
                email: email,
                password: password,
            })
            .send();
        return response;
    }
}
