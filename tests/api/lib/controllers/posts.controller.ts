import { ApiRequest } from "../request";

const baseUrl: string = global.appConfig.baseUrl;

export class PostsController {
    async createPost(authorId: number, previewImage: string, body: string, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts`)
            .body({
                authorId,
                previewImage,
                body
            })
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async getAllPosts() {
        const response = await new ApiRequest().prefixUrl(baseUrl).method("GET").url(`api/Posts`).send();
        return response;
    }

    async likePost(entityId: number, isLike: boolean, userId: number, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts/like`)
            .body({
                entityId,
                isLike,
                userId
            })
            .bearerToken(accessToken)
            .send();
        return response;
    }
}
