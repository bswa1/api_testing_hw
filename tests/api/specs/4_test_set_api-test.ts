import { checkResponseTime, checkStatusCode } from "../../helpers/functionsForChecking.helper";
import { RegisterController } from "../lib/controllers/register.controller";
import { faker } from "@faker-js/faker";

const register = new RegisterController();

describe("Use test data set for registration", () => {
    let invalidRegistrationCredentialsDataSet = [
        { email: "qa.test@gmail.com", password: "" },
        { email: "qa.test@gmail.com", password: "123" },
        { email: "qa.test@gmail.com", password: "12345678901234567" },
        { email: "qa.test.gmail.com", password: "ValidPassword" },
        { email: "", password: "ValidPassword" },
        { email: "@", password: "ValidPassword" },
    ];

    const userName = faker.internet.userName();
    const avatar = faker.image.avatar();

    invalidRegistrationCredentialsDataSet.forEach((credentials) => {
        it(`should not register user with invalid credentials : '${credentials.email}' + '${credentials.password}'`, async () => {
            let response = await register.register(0, avatar, credentials.email, credentials.password, userName);

            checkStatusCode(response, 400);
            checkResponseTime(response, 1000);
        });
    });
});
