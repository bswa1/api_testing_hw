import { expect } from "chai";
import { AuthController } from "../lib/controllers/auth.controller";
import { RegisterController } from "../lib/controllers/register.controller";
import { checkResponseTime, checkStatusCode } from "../../helpers/functionsForChecking.helper";
import { faker } from "@faker-js/faker";

const auth = new AuthController();
const register = new RegisterController();

describe("Registration and Login Negative Tests", () => {
    it(`should not allow registration with invalid email`, async () => {
        const invalidEmail = faker.internet.email().replace("@", ".");
        const password = faker.internet.password({
            length: faker.number.int({ min: 4, max: 16 }),
            pattern: /[a-zA-Z0-9]/,
        });
        const userName = faker.internet.userName();
        const avatar = faker.image.avatar();

        const registrationResponse = await register.register(0, avatar, invalidEmail, password, userName);

        checkStatusCode(registrationResponse, 400);
        expect(registrationResponse.body.errors).to.exist;
        expect(registrationResponse.body.errors[0]).to.equal("'Email' is not a valid email address.");
        checkResponseTime(registrationResponse, 1000);
    });

    it("should not allow registration with invalid password - 17 characters", async () => {
        const email = faker.internet.email();
        const invalidPassword = faker.internet.password({
            length: 17,
            pattern: /[a-zA-Z0-9]/,
        });
        const userName = faker.internet.userName();
        const avatar = faker.image.avatar();

        const registrationResponse = await register.register(0, avatar, email, invalidPassword, userName);

        checkStatusCode(registrationResponse, 400);
        expect(registrationResponse.body.errors).to.exist;
        expect(registrationResponse.body.errors[0]).to.equal("Password must be from 4 to 16 characters.");
        checkResponseTime(registrationResponse, 1000);
    });

    it("should return an error during authorisation when email doesn't exist", async () => {
        const email = faker.internet.email();
        const password = faker.internet.password({
            length: 10,
            pattern: /[a-zA-Z0-9]/,
        });

        const loginResponse = await auth.login(email, password);

        checkStatusCode(loginResponse, 404);
        expect(loginResponse.body.error).to.exist;
        expect(loginResponse.body.error).to.equal("Entity User was not found.");
    });
});
