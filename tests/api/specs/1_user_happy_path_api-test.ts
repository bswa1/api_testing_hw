import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { RegisterController } from "../lib/controllers/register.controller";
import { checkResponseTime, checkStatusCode, checkJsonSchema } from "../../helpers/functionsForChecking.helper";
import { faker } from "@faker-js/faker";

const schemas = require('./data/schemas_testData.json');

const users = new UsersController();
const auth = new AuthController();
const register = new RegisterController();

let registrationEmail: string;
let registrationPassword: string;
let registrationAvatar: string;
let registrationUserName: string;

let updateEmail: string;
let updatePassword: string;
let updateAvatar: string;
let updateUserName: string;

let registeredUserId: number;
let accessToken: string;
let registrationResponse: any;
let loginResponse: any;

describe("User Registration and Delete Happy Path", () => {
    before("User Registration and login", async () => {
        registrationEmail = faker.internet.email();
        registrationPassword = faker.internet.password({
            length: faker.number.int({ min: 4, max: 16 }),
            pattern: /[a-zA-Z0-9]/,
        });
        registrationUserName = faker.internet.userName();
        registrationAvatar = faker.image.avatar();

        registrationResponse = await register.register(
            0,
            registrationAvatar,
            registrationEmail,
            registrationPassword,
            registrationUserName
        );
        registeredUserId = registrationResponse.body.user.id;

        loginResponse = await auth.login(registrationEmail, registrationPassword);
        accessToken = loginResponse.body.token.accessToken.token;
    });

    it(`should register user with provided details`, async () => {
        expect(registrationResponse).to.exist;
        let registrationData = registrationResponse.body.user;

        expect(registrationData.id).to.not.equal(0);
        expect(registrationData.avatar).to.equal(registrationAvatar);
        expect(registrationData.email).to.equal(registrationEmail);
        expect(registrationData.userName).to.equal(registrationUserName);

        checkStatusCode(registrationResponse, 201);
        checkResponseTime(registrationResponse, 1000);
        checkJsonSchema(registrationResponse.body, schemas.schema_register);
    });

    it("should return all users", async () => {
        const response = await users.getAllUsers();

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        expect(response.body.length).to.be.greaterThan(0);
    });

    it(`should return correct details after log in`, async () => {
        expect(loginResponse).to.exist;
        let loginData = loginResponse.body.user;

        expect(loginData.id).to.equal(registeredUserId);
        expect(loginData.avatar).to.equal(registrationAvatar);
        expect(loginData.email).to.equal(registrationEmail);
        expect(loginData.userName).to.equal(registrationUserName);

        checkStatusCode(loginResponse, 200);
        checkResponseTime(loginResponse, 1000);
        checkJsonSchema(loginResponse.body, schemas.schema_login);
    });

    it("should return current user details", async () => {
        const response = await users.getCurrentUser(accessToken);

        let userData = loginResponse.body.user;

        expect(userData.id).to.equal(registeredUserId);
        expect(userData.avatar).to.equal(registrationAvatar);
        expect(userData.email).to.equal(registrationEmail);
        expect(userData.userName).to.equal(registrationUserName);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkJsonSchema(response.body, schemas.schema_user);
    });

    it("should update current user", async () => {
        updateEmail = faker.internet.email();
        updatePassword = faker.internet.password({
            length: faker.number.int({ min: 4, max: 16 }),
            pattern: /[a-zA-Z0-9]/,
        });
        updateUserName = faker.internet.userName();
        updateAvatar = faker.image.avatar();

        const response = await users.updateUser(
            {
                id: registeredUserId,
                avatar: updateAvatar,
                email: updateEmail,
                userName: updateUserName,
            },
            accessToken
        );
        
        checkStatusCode(response, 204);
        checkResponseTime(response, 1000);
    });

    it("should return current user details after update", async () => {
        const response = await users.getCurrentUser(accessToken);

        let userData = response.body;

        expect(userData.id).to.equal(registeredUserId);
        expect(userData.avatar).to.equal(updateAvatar);
        expect(userData.email).to.equal(updateEmail);
        expect(userData.userName).to.equal(updateUserName);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
    });

    it("should return user by id", async () => {
        const response = await users.getUserById(registeredUserId);

        let userData = response.body;

        expect(userData.id).to.equal(registeredUserId);
        expect(userData.avatar).to.equal(updateAvatar);
        expect(userData.email).to.equal(updateEmail);
        expect(userData.userName).to.equal(updateUserName);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
    });

    after("Delete current user", async () => {
        const response = await users.deleteUser(registeredUserId, accessToken);
        checkStatusCode(response, 204);
        checkResponseTime(response, 1000);
    });
});
