import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { RegisterController } from "../lib/controllers/register.controller";
import { checkResponseTime, checkStatusCode, checkJsonSchema } from "../../helpers/functionsForChecking.helper";
import { faker } from "@faker-js/faker";
import { PostsController } from "../lib/controllers/posts.controller";

const schemas = require('./data/schemas_testData.json');

const auth = new AuthController();
const register = new RegisterController();
const posts = new PostsController();
const users = new UsersController();

let registrationEmail: string;
let registrationPassword: string;
let registrationAvatar: string;
let registrationUserName: string;

let registeredUserId: number;
let accessToken: string;
let postBody: string;
let previewImage: string;
let postId: number;

describe("Happy Path Tests post", () => {
    before("User Registration and login", async () => {
        registrationEmail = faker.internet.email();
        registrationPassword = faker.internet.password({
            length: faker.number.int({ min: 4, max: 16 }),
            pattern: /[a-zA-Z0-9]/,
        });
        registrationUserName = faker.internet.userName();
        registrationAvatar = faker.image.avatar();

        const registrationResponse = await register.register(
            0,
            registrationAvatar,
            registrationEmail,
            registrationPassword,
            registrationUserName
        );
        registeredUserId = registrationResponse.body.user.id;

        const loginResponse = await auth.login(registrationEmail, registrationPassword);
        accessToken = loginResponse.body.token.accessToken.token;

        checkStatusCode(loginResponse, 200);
        checkStatusCode(registrationResponse, 201);
    });

    it("should create post", async () => {
        postBody = faker.lorem.paragraph({ min: 1, max: 3});
        previewImage = faker.image.url();

        const response = await posts.createPost(registeredUserId, previewImage, postBody, accessToken);
        postId = response.body.id;

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkJsonSchema(response.body, schemas.schema_post);

        expect(response.body.body).to.equal(postBody);

        const allPostsResponse = await posts.getAllPosts();
        const allPosts = allPostsResponse.body;
        const createdPost = allPosts.find((post: any) => post.id === postId);

        expect(createdPost).to.exist;
        expect(createdPost.author.id).to.equal(registeredUserId);
        expect(createdPost.previewImage).to.equal(previewImage);
        expect(createdPost.body).to.equal(postBody);
    });

    it("should like post", async () => {
        const response = await posts.likePost(postId, true, registeredUserId, accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);

        const allPostsResponse = await posts.getAllPosts();
        const allPosts = allPostsResponse.body;
        const post = allPosts.find((post: any) => post.id === postId);
        const addedLike = post.reactions.find((like: any) => like.user.id === registeredUserId);

        expect(addedLike).to.exist;
        expect(addedLike.isLike).to.be.true;
    });

    after("Delete current user", async () => {
        const response = await users.deleteUser(registeredUserId, accessToken);
        checkStatusCode(response, 204);
        checkResponseTime(response, 1000);
    });
});
